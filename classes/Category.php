<?php
	/*######################################
	 	Include or add file code Start
	########################################*/
     $filepath = realpath(dirname(__FILE__));
     include_once ($filepath.'/../lib/Database.php');
     include_once ($filepath.'/../helpers/Format.php');

     /*### We use this process to remove file path error ###*/
	 /*include_once '../lib/Database.php'; 
	 include_once '../helpers/Format.php';*/ 
	/*######################################
	 	Include or add file code end
	########################################*/
 ?>

<?php
class Category{
	
		/*Create a variable to call dtabase class */
		private $db;
		/*Create a variable to call Format class */
		private $fm;

		public function __construct(){
			$this->db = new Database();
			$this->fm = new Format();
		}




		/*##############################################################
			Task1 query code start
		##############################################################*/
		public function getAllCat(){
			// $query = "SELECT * FROM tbl_category ORDER BY catId DESC";
			$query = "SELECT category.Name as categoryName, count(item_category_relations.ItemNumber) as TotalItems FROM category LEFT JOIN item_category_relations ON category.Id=item_category_relations.categoryId group by category.Id ORDER BY count(item_category_relations.ItemNumber) DESC";
			$result = $this->db->select($query);
			return $result;
		}
		/*##############################################################
			end
		##############################################################*/






        /*############################################################################
              Category retieve function code start
        #############################################################################*/
// function categories()
// {

	
// 	$sql = "SELECT * FROM categories WHERE Disabled=0";
// 	$result = $conn->query($sql);
	
// 	$categories = array();
	
// 	while($row = $result->fetch_assoc())
// 	{
// 		$categories[] = array(
// 			'id' => $row['id'],
// 			'Disabled' => $row['Disabled'],
// 			'Name' => $row['Name'],
// 			'subcategory' => sub_categories($row['id']),
// 		);
// 	}
	
// 	return $categories;

// }








// function sub_categories($id)
// {	

	
// 	$sql = "SELECT * FROM catetory_relations WHERE ParentcategoryId=$id";
// 	$result = $conn->query($sql);
	
// 	$categories = array();
	
// 	while($row = $result->fetch_assoc())
// 	{
// 		$categories[] = array(
// 			'Id' => $row['id'],
// 			'ParentcategoryId' => $row['ParentcategoryId'],
// 			'Name' => $row['Name'],
// 			'categoryId' => sub_categories($row['Id']),
// 		);
// 	}
// 	return $categories;
// }





// function viewsubcat($categories)
// {
// 	$html = '<ul class="sub-category">';
// 	foreach($categories as $category){

// 		$html .= '<li>'.$category['Name'].'</li>';
		
// 		if( ! empty($category['subcategory'])){
// 			$html .= viewsubcat($category['subcategory']);
// 		}
// 	}
// 	$html .= '</ul>';
	
// 	return $html;
// }


     /*############################################################################
             Category retieve function code end
        #############################################################################*/
























        /*############################################################################
              Category retieve function code start
        #############################################################################*/
        public function productByCat($id){
            $catId = mysqli_real_escape_string($this->db->link, $id);
            $query = "SELECT * FROM item_category_relations WHERE catId = '$catId'";
            $result = $this->db->select($query);
            return $result;
        }
        /*############################################################################
             Category retieve function code end
        #############################################################################*/





}
?>